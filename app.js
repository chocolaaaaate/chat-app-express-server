var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = 80;

server.listen(port, () => console.log("listening"));
// WARNING: app.listen(80) will NOT work here!

io.on('connection', (client) => {
    client.on('chat', (chatMessage) => {
        io.sockets.emit('chat', chatMessage); // notify other clients listening to this chat
    });

    client.on('typing', (typingNotificatioFromClient) => {
        client.broadcast.emit('typing',
            {
                who: typingNotificatioFromClient.who,
                isTyping: typingNotificatioFromClient.isTyping
            }
        );
    })
});
